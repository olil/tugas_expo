import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    FlatList,
} from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import data from '../skillData.json'
//import SkillComponent from './components/SkillComponent'

export default class Skill extends Component {
    render = () => {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')} style={{ width: 98, height: 30 }}/>
                </View>
                <View style={styles.profile}>
                    <Ionicons name="md-contact" size={40} color="#B4E9FF" />
                    <View>
                        <Text style={styles.profileText}>Hai,</Text>
                        <Text style={styles.nameText}>Kholilur Rohman</Text>
                    </View>
                </View>
                <View style={styles.titleSkill}>
                    <Text style={styles.titleText}>SKILL</Text>
                    <View style={{ height: 4, backgroundColor: "#3EC6FF"}}></View>
                </View>
                <View style={styles.tabSkill}>
                    <TouchableOpacity>
                        <View style={styles.tabTitle}>
                            <Text style={styles.tabTitle}>Framework</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.tabTitle}>
                            <Text style={styles.tabTitle}>Bahasa Pemrograman</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.tabTitle}>
                            <Text style={styles.tabTitle}>Teknologi</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerBody}>
                    <FlatList 
                        data={data.items} 
                        renderItem={(data) => 
                            <skillComponent skill={data.item}/>
                        }
                        keyExtractor={(data) => data.id.toString()}
                        ItemSeparatorComponent={() => <View style={{height:4,backgroundColor:'#fff'}}/>}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        backgroundColor: '#0071F6'
    },
    logo: {
        alignItems: "flex-end",
    },
    profile: {
        flexDirection: 'row',
        paddingHorizontal: 19,
        alignItems: 'center'
    },
    profileText: {
        fontSize: 12,
        marginLeft: 11
    },
    nameText: {
        fontSize: 16,
        color: "#003366",
        marginLeft: 11
    },
    titleSkill: {
        paddingHorizontal: 19,
        marginBottom: 10,
        color : 'white'
    },
    titleText: {
        fontSize: 36,
        marginTop: 16,
        color: "#003366"
    },
    tabSkill: {
        flexDirection: 'row',
        paddingHorizontal: 19,
        
 
    },
    tabTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'white',
        marginRight: 6,
        padding: 5,
        borderRadius: 8,
        backgroundColor: '#083366',
    },
    containerBody: {
        flex: 1,
        marginHorizontal: 15,
        borderRadius: 8,
        backgroundColor: 'white',
        marginVertical: 10,
    },
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 12,
        paddingHorizontal: 10,
        backgroundColor: 'blue'
    },
    nameSkill: {
        fontSize: 24,
        color: '#003366',
    },
    tab: {
        fontSize: 16,
        backgroundColor: 'blue'
    },
    presentage: {
        fontSize: 48,
        color: 'blue',
        textAlign: 'right'
    }
})
