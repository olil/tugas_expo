import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from './images/logo.png';

const { width: WIDTH } = Dimensions.get('window')
export default class RegistrationScreen extends Component {
  render() {
    return (
      <View style={styles.container} >
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
          <Text style={styles.TextTitile}>REGISTRATION</Text>
        </View>

        <View style={styles.inputContainer}>
          <Icon name="person" size={40} color={'#FFDE89'} style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder={'Username / Email'}
            placeholderTextColor={'#FFDE89'}
            underlineColorAndroid='transparent'
          />
        </View>

        <View style={styles.inputContainer}>
          <Icon name="lock" size={40} color={'#FFDE89'} style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder={'Password'}
            secureTextEntry={true}
            placeholderTextColor={'#FFDE89'}
            underlineColorAndroid='transparent'
          />

          <TouchableOpacity style={styles.btnEye}>
            <Icon name={'remove-red-eye'} size={26} color={'#FFDE89'} />
          </TouchableOpacity>
        </View>

        <View style={styles.inputContainer}>
          <Icon name="lock" size={40} color={'#FFDE89'} style={styles.inputIcon} />
          <TextInput
            style={styles.input}
            placeholder={'Confirm Password'}
            secureTextEntry={true}
            placeholderTextColor={'#FFDE89'}
            underlineColorAndroid='transparent'
          />

          <TouchableOpacity style={styles.btnEye}>
            <Icon name={'remove-red-eye'} size={26} color={'#FFDE89'} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.btnRegistration}>
            <Text style={styles.textRegistration} color={'#0071F6'}>Registration</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0071F6',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoContainer: {
    alignItems: 'center',
    marginBottom: 50
  },
  logo: {
    width: 300,
    height: 70
  },
  TextTitile: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 10,
    opacity: 0.5
  },
  inputContainer: {
    marginTop: 10
  },
  input: {
    width: WIDTH - 105,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 70,
    backgroundColor: '#7DABF1',
    color: '#FFDE89',
    marginHorizontal: 25
  },
  inputIcon: {
    position: 'absolute',
    top: 8,
    left: 37
  },
  btnEye: {
    position: 'absolute',
    top: 8,
    right: 37
  },
  btnRegistration:{
    width: WIDTH - 105,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    backgroundColor: 'blue',
    justifyContent: 'center',
    marginTop: 20,
    width: WIDTH - 300
  },
  textRegistration:{
    color: 'white',
    fontSize: 16,
    textAlign: 'center'
  },
});