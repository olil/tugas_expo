import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from './images/logo.png';

const { width: WIDTH } = Dimensions.get('window')
export default class LoginaScreen extends Component {
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.logoContainer}>
                    <Image source={logo} style={styles.logo} />
                    <Text style={styles.TextTitile}>LOGIN</Text>
                </View>

                <View style={styles.inputContainer}>
                    <Icon name="person" size={40} color={'#083366'} style={styles.inputIcon} />
                    <TextInput
                        style={styles.input}
                        placeholder={'Username / Email'}
                        placeholderTextColor={'#083366'}
                        underlineColorAndroid='transparent'
                    />
                </View>

                <View style={styles.inputContainer}>
                    <Icon name="lock" size={40} color={'#083366'} style={styles.inputIcon} />
                    <TextInput
                        style={styles.input}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        placeholderTextColor={'#083366'}
                        underlineColorAndroid='transparent'
                    />

                    <TouchableOpacity style={styles.btnEye}>
                        <Icon name={'remove-red-eye'} size={26} color={'#083366'} />
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.btnLogin}>
                    <Text style={styles.textLogin} color={'#0071F6'}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.textRegistration}>
                    <Text style={styles.textRegistration} color={'#0071F6'}>Registration..?</Text>
                </TouchableOpacity>

                <View style={styles.textLoginOther}>
                    <Text style={styles.textLoginOther}>Login with Other :</Text>
                </View>

                <View style={styles.loginOther}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./images/facebook.png')} style={{ width: 70, height: 70 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./images/instagram.png')} style={{ width: 70, height: 70 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./images/google.png')} style={{ width: 70, height: 70 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./images/twitter.png')} style={{ width: 70, height: 70 }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0071F6',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoContainer: {
        alignItems: 'center',
        marginBottom: 20
    },
    logo: {
        width: 300,
        height: 70,
        marginBottom: 40
    },
    TextTitile: {
        marginTop: 20,
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 5,
        opacity: 0.5
    },
    inputContainer: {
        marginTop: 10
    },
    input: {
        width: WIDTH - 105,
        height: 45,
        borderRadius: 25,
        fontSize: 16,
        paddingLeft: 70,
        backgroundColor: '#7DABF1',
        color: '#FFDE89',
        marginHorizontal: 25
    },
    inputIcon: {
        position: 'absolute',
        top: 8,
        left: 37
    },
    btnEye: {
        position: 'absolute',
        top: 8,
        right: 37
    },
    btnLogin: {
        width: WIDTH - 105,
        height: 45,
        borderRadius: 25,
        fontSize: 16,
        backgroundColor: '#083366',
        justifyContent: 'center',
        marginTop: 20,
        width: WIDTH - 300
    },
    textLogin: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    textRegistration: {
        color: 'red',
        marginTop: 15,
        fontSize: 19,
        fontWeight: 'bold',
        textAlign: 'right'
    },
    textLoginOther: {
        color: 'black',
        marginTop: 30,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    loginOther: {
        height: 60,
        marginTop: 20,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-evenly'
      }
});