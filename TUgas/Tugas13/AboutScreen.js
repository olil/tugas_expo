import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from './images/logo.png';
import person from './images/person.png';
import facebook from './images/facebook.png';
import instagram from './images/instagram.png';
import twitter from './images/twitter.png';
import gitlab from './images/gitlab.png';
import github from './images/github.png';
import home from './images/home.png';


const { width: WIDTH } = Dimensions.get('window')
export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.personContainer}>
                    <Text style={styles.about}>ABOUT ME</Text>
                </View>

                <View style={styles.personContainer}>
                    <Image source={person} style={styles.person} />
                    <Text style={styles.TextName}>KHOLILUR ROHMAN</Text>
                    <Text style={styles.Textjob}>Android Developer</Text>
                </View>
                            
                <View style={styles.sosialMedia}>
                    <Text style={styles.sosialMedia}>Sosial Media</Text>
                </View>

                <View style={styles.facebookContainer}>
                    <Image source={facebook} style={styles.imgSosmed} />
                    <TouchableOpacity styles={styles.textSosmed}>
                        <Text style={styles.textSosmed} size={40}>www.facebook.com/olil</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.instagramContainer}>
                    <Image source={instagram} style={styles.imgSosmed} />
                    <TouchableOpacity styles={styles.textSosmed}>
                        <Text style={styles.textSosmed} size={40}>www.instagram.com/olil29</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.instagramContainer}>
                    <Image source={twitter} style={styles.imgSosmed} />
                    <TouchableOpacity styles={styles.textSosmed}>
                        <Text style={styles.textSosmed} size={40}>www.twitter.com/olil_</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.sosialMedia}>
                    <Text style={styles.sosialMedia}>Sosial Media</Text>
                </View>

                <View style={styles.instagramContainer}>
                    <TouchableOpacity styles={styles.textSosmed}>
                        <Image source={gitlab} style={styles.imgSosmed} />
                    </TouchableOpacity>
                </View>

                <View style={styles.instagramContainer}>
                    <TouchableOpacity styles={styles.textSosmed}>
                        <Image source={github} style={styles.imgSosmed} />
                    </TouchableOpacity>
                </View>
            
                <View style={styles.personContainer}>
                    <TouchableOpacity>
                    <Image source={home} style={styles.home} />
                    </TouchableOpacity>
                </View>
                        
                    {/* <View style={styles.data}>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }}>Name</Text>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 16 }}>Will Smith</Text>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }}>Bord Date</Text>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 16 }}>12-Agustus-1995</Text>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }} >My Social Media</Text>
                        <Text style={{ fontFamily: "Roboto", fontWeight: "bold", fontSize: 18, color: "#AAA6A6" }}>My Portofolio</Text>
                    </View> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FE8900',
        justifyContent: 'center'
    },
    about: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 50,
        marginLeft: 10
    },
    personContainer: {
        alignItems: 'center',
        marginBottom: 20
      },
      person: {
        width: 150,
        height: 150,
      },
      TextTitile: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 3
      },
      TextName: {
        color: '#0071F6',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 3,
        opacity: 0.7,
        marginLeft: 10
      },
      Textjob: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 3,
        textAlign: 'center',
        marginBottom: 20
      },
      sosialMedia: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 3,
        opacity: 0.7,
        marginLeft: 10
      },
      imgSosmed: {
        width: 50,
        height: 50,
        marginLeft: 10
      },
      textSosmed: {
          color: '#0071F6',
          marginLeft: 20,
      },
      home: {
          height: 40,
          width: 40,
          marginTop: 15
      }
});
